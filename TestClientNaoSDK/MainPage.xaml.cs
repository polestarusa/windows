﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Com.PoleStar.NaoSdk.Api.External;
using WrapperNaoSDK;
using Windows.Storage;

namespace TestClientNaoSDK
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();
            NAOServicesConfig.UploadDone += NAOServicesConfig_UploadDone;
            InitClient1();
            InitClient2();
        }

        private void SendLogButton_OnClick(object sender, RoutedEventArgs e)
        {
            SendLogButton.IsEnabled = false;
            textClient1 = $"{DateTime.Now} | Upload in progress...\n" + textClient1;
            StatusTextClient1.Text = textClient1;
            MainProgressBar.Visibility = Visibility.Visible;
            NAOServicesConfig.UploadNAOLogInfo($"Sent from Windows 10 app - {DateTime.Now}");
        }

        private async void NAOServicesConfig_UploadDone(object sender, NaoStateEventArgs e)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
             {
                 if (e.State)
                     textClient1 = $"{DateTime.Now} | Upload has succeeded\n" + textClient1;
                 else
                     textClient1 = $"{DateTime.Now} | Upload failed\n" + textClient1;
                 StatusTextClient1.Text = textClient1;
                 MainProgressBar.Visibility = Visibility.Collapsed;
                 SendLogButton.IsEnabled = true;
             });
        }

        #region Client 1

        private NAOLocationHandle locationHandleClient1;
        private string textClient1 = string.Empty;

        private void InitClient1()
        {
            locationHandleClient1 = new NAOLocationHandle("00eDGm7h9Jlmof8IWm5nhA");
            locationHandleClient1.OnSynchronizationFailed += NaoLocationHandleClient1_SynchronizationFailed;
            locationHandleClient1.OnSynchronizationSucceeded += NaoLocationHandleClient1_SynchronizationSucceeded;
            locationHandleClient1.OnLocationChanged += LocationHandleClient1_LocationChanged;
            locationHandleClient1.OnStatusChanged += LocationHandleClient1_OnStatusChanged;
            locationHandleClient1.OnEnterSite += LocationHandleClient1_EnterSite;
            locationHandleClient1.OnExitSite += LocationHandleClient1_ExitSite;
            locationHandleClient1.OnError += LocationHandleClient1_Error;

            // Sensor Request Listener
            locationHandleClient1.OnCompassCalibrated += LocationHandleClient1_CompassCalibrated;

            locationHandleClient1.OnRequestedCompassCalibration += LocationHandleClient1_RequestedCompassCalibration;

            locationHandleClient1.OnRequestedBle += LocationHandleClient1_RequestedBle;

            locationHandleClient1.OnRequestedLocation += LocationHandleClient1_RequestedLocation;

            locationHandleClient1.OnBleActivated += LocationHandleClient1_BleActivated;

            locationHandleClient1.OnLocationActivated += LocationHandleClient1_LocationActivated;
        }

        private void LocationHandleClient1_LocationActivated(object sender, NaoGlobalEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient1 = $"{DateTime.Now} | LocationActivated received\n" + textClient1;
                StatusTextClient1.Text = textClient1;
            });
        }

        private void LocationHandleClient1_BleActivated(object sender, NaoGlobalEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient1 = $"{DateTime.Now} | BleActivated received\n" + textClient1;
                StatusTextClient1.Text = textClient1;
            });
        }

        private void LocationHandleClient1_RequestedLocation(object sender, NaoMessageEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient1 = $"{DateTime.Now} | RequestedLocation received\n" + textClient1;
                StatusTextClient1.Text = textClient1;
            });
        }

        private void LocationHandleClient1_RequestedBle(object sender, NaoMessageEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient1 = $"{DateTime.Now} | RequestedBle received\n" + textClient1;
                StatusTextClient1.Text = textClient1;
            });
        }

        private void LocationHandleClient1_RequestedCompassCalibration(object sender, NaoMessageEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient1 = $"{DateTime.Now} | CompassCalibrated received\n" + textClient1;
                StatusTextClient1.Text = textClient1;
            });
        }

        private void LocationHandleClient1_CompassCalibrated(object sender, NaoGlobalEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient1 = $"{DateTime.Now} | RequestedCompassCalibration received\n" + textClient1;
                StatusTextClient1.Text = textClient1;
            });
        }

        private void LocationHandleClient1_Error(object sender, NaoErrorEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient1 = $"{DateTime.Now} | Error received Code:{args.Code.ToString() } | Message:{args.Message}\n" + textClient1;
                StatusTextClient1.Text = textClient1;
            });
        }

        private void LocationHandleClient1_ExitSite(object sender, NaoSiteEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient1 = $"{DateTime.Now} | Exit site succeeded:{args.SiteName }\n" + textClient1;
                StatusTextClient1.Text = textClient1;
            });
        }

        private void StartButtonClient1_OnClick(object sender, RoutedEventArgs e)
        {
            SendLogButton.IsEnabled = false;
            StartButtonClient1.IsEnabled = false;
            textClient1 = $"{DateTime.Now} | Waiting for synchronization...\n" + textClient1;
            textClient1 = $"{DateTime.Now} | Waiting for Status Changed...\n" + textClient1;
            textClient1 = $"{DateTime.Now} | Waiting for Error...\n" + textClient1;
            textClient1 = $"{DateTime.Now} | Waiting for Compass Calibrated...\n" + textClient1;
            textClient1 = $"{DateTime.Now} | Waiting for Requested Compass Calibration...\n" + textClient1;
            textClient1 = $"{DateTime.Now} | Waiting for Requested Ble...\n" + textClient1;
            textClient1 = $"{DateTime.Now} | Waiting for Requested Location...\n" + textClient1;
            textClient1 = $"{DateTime.Now} | Waiting for Ble Activated...\n" + textClient1;
            textClient1 = $"{DateTime.Now} | Waiting for Location Activated...\n" + textClient1;

            StatusTextClient1.Text = textClient1;
            MainProgressBar.Visibility = Visibility.Visible;
            locationHandleClient1.SynchronizeData();
        }

        private void LocationHandleClient1_EnterSite(object sender, NaoSiteEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient1 = $"{DateTime.Now} | Enter site succeeded:{args.SiteName }\n" + textClient1;
                StatusTextClient1.Text = textClient1;
            });
        }

        private void LocationHandleClient1_OnStatusChanged(object sender, NaoStatusChangedEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient1 = $"{DateTime.Now} | Status changed:{args.Status.ToString() }\n" + textClient1;
                StatusTextClient1.Text = textClient1;
            });
        }

        private void LocationHandleClient1_LocationChanged(object sender, NaoLocationEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient1 = $"{DateTime.Now} | {args.Longitude} - {args.Latitude} - {args.Altitude} - {args.Heading}\n" + textClient1;
                StatusTextClient1.Text = textClient1;
                MainProgressBar.Visibility = Visibility.Collapsed;
            });
        }

        private void NaoLocationHandleClient1_SynchronizationSucceeded(object sender, NaoEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient1 = $"{DateTime.Now} | Synchronization succeeded\n" + textClient1;
                StatusTextClient1.Text = textClient1;
                MainProgressBar.Visibility = Visibility.Collapsed;

                locationHandleClient1.Start();
                StopButtonClient1.IsEnabled = true;
                textClient1 = $"{DateTime.Now} | Handle started\n" + textClient1;
                StatusTextClient1.Text = textClient1;
                DatabaseVersionText.Text = NAOServicesConfig.getSoftwareVersion() + ":" + locationHandleClient1.GetDatabaseVersions();
                textClient1 = $"{DateTime.Now} | Waiting for location...\n" + textClient1;
                textClient1 = $"{DateTime.Now} | Waiting for Enter site...\n" + textClient1;
                textClient1 = $"{DateTime.Now} | Waiting for Exit site...\n" + textClient1;

                StatusTextClient1.Text = textClient1;
                MainProgressBar.Visibility = Visibility.Visible;
            });
        }

        private void NaoLocationHandleClient1_SynchronizationFailed(object sender, NaoSynchronizationFailedEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient1 = $"{DateTime.Now} | Synchronization failed: {args.Message} ({args.Code})\n" + textClient1;
                StatusTextClient1.Text = textClient1;
                MainProgressBar.Visibility = Visibility.Collapsed;
                StartButtonClient1.IsEnabled = true;

            });
        }

        private void StopButtonClient1_OnClick(object sender, RoutedEventArgs e)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (locationHandleClient1 != null)
                {
                    locationHandleClient1.Stop();
                    SendLogButton.IsEnabled = true;
                    textClient1 = $"{DateTime.Now} | Handle stopped\n" + textClient1;
                    StopButtonClient1.IsEnabled = false;
                    StartButtonClient1.IsEnabled = true;
                    MainProgressBar.Visibility = Visibility.Collapsed;
                }
                else
                {
                    StatusTextClient1.Text = $"{DateTime.Now} | Handle must be started\n" + textClient1;
                }
                StatusTextClient1.Text = textClient1;
            });
        }

        private void ClearButtonClient1_OnClick(object sender, RoutedEventArgs e)
        {
            textClient1 = string.Empty;
            StatusTextClient1.Text = string.Empty;
        }

        #endregion

        #region Client 2

        private NAOLocationHandle locationHandleClient2;
        private string textClient2 = string.Empty;

        private void InitClient2()
        {
            // Correct key
            locationHandleClient2 = new NAOLocationHandle("00eDGm7h9Jlmof8IWm5nhA");
            locationHandleClient2.OnSynchronizationFailed += NaoLocationHandleClient2_SynchronizationFailed;
            locationHandleClient2.OnSynchronizationSucceeded += NaoLocationHandleClient2_SynchronizationSucceeded;
            locationHandleClient2.OnLocationChanged += LocationHandleClient2_LocationChanged;
            locationHandleClient2.OnError += LocationHandleClient2_Error;
        }

        private void LocationHandleClient2_Error(object sender, NaoErrorEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient2 = $"{DateTime.Now} | Error received Code:{args.Code.ToString() } | Message:{args.Message}\n" + textClient2;
                StatusTextClient2.Text = textClient2;
            });
        }

        private void StartButtonClient2_OnClick(object sender, RoutedEventArgs e)
        {
            SendLogButton.IsEnabled = false;
            StartButtonClient2.IsEnabled = false;
            textClient2 = $"{DateTime.Now} | Waiting for synchronization...\n" + textClient2;
            textClient2 = $"{DateTime.Now} | Waiting for Status Changed...\n" + textClient2;
            textClient2 = $"{DateTime.Now} | Waiting for Enter site...\n" + textClient2;

            textClient2 = $"{DateTime.Now} | Waiting for Exit site...\n" + textClient2;

            textClient2 = $"{DateTime.Now} | Waiting for Error...\n" + textClient2;

            StatusTextClient2.Text = textClient2;
            MainProgressBar.Visibility = Visibility.Visible;
            locationHandleClient2.SynchronizeData();
            locationHandleClient2.OnStatusChanged += LocationHandleClient2_StatusChanged;

            locationHandleClient2.OnEnterSite += LocationHandleClient2_EnterSite;
            locationHandleClient2.OnExitSite += LocationHandleClient2_ExitSite;
        }

        private void LocationHandleClient2_ExitSite(object sender, NaoSiteEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient2 = $"{DateTime.Now} | Exit site succeeded:{args.SiteName }\n" + textClient2;
                StatusTextClient2.Text = textClient2;
            });
        }

        private void LocationHandleClient2_EnterSite(object sender, NaoSiteEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient2 = $"{DateTime.Now} | Enter site succeeded:{args.SiteName }\n" + textClient2;
                StatusTextClient2.Text = textClient2;
            });
        }

        private void LocationHandleClient2_StatusChanged(object sender, NaoStatusChangedEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient2 = $"{DateTime.Now} | Status changed:{args.Status.ToString() }\n" + textClient2;
                StatusTextClient2.Text = textClient2;
            });
        }

        private void LocationHandleClient2_LocationChanged(object sender, NaoLocationEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient2 = $"{DateTime.Now} | {args.Longitude} - {args.Latitude} - {args.Altitude} - {args.Heading}\n" + textClient2;
                StatusTextClient2.Text = textClient2;
                MainProgressBar.Visibility = Visibility.Collapsed;
            });
        }

        private void NaoLocationHandleClient2_SynchronizationSucceeded(object sender, WrapperNaoSDK.NaoEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient2 = $"{DateTime.Now} | Synchronization succeeded\n" + textClient2;
                StatusTextClient2.Text = textClient2;
                MainProgressBar.Visibility = Visibility.Collapsed;

                locationHandleClient2.Start();
                StopButtonClient2.IsEnabled = true;
                textClient2 = $"{DateTime.Now} | Handle started\n" + textClient2;
                DatabaseVersionText.Text = locationHandleClient1.GetDatabaseVersions();
                textClient2 = $"{DateTime.Now} | Waiting for location...\n" + textClient2;
                StatusTextClient2.Text = textClient2;
                MainProgressBar.Visibility = Visibility.Visible;

            });
        }

        private void NaoLocationHandleClient2_SynchronizationFailed(object sender, WrapperNaoSDK.NaoSynchronizationFailedEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                textClient2 = $"{DateTime.Now} | Synchronization failed: {args.Message} ({args.Code})\n" + textClient2;
                StatusTextClient2.Text = textClient2;
                MainProgressBar.Visibility = Visibility.Collapsed;
                StartButtonClient2.IsEnabled = true;
            });
        }

        private void StopButtonClient2_OnClick(object sender, RoutedEventArgs e)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (locationHandleClient2 != null)
                {
                    locationHandleClient2.Stop();
                    SendLogButton.IsEnabled = true;
                    textClient2 = $"{DateTime.Now} | Handle stopped\n" + textClient2;
                    StopButtonClient2.IsEnabled = false;
                    StartButtonClient2.IsEnabled = true;
                    MainProgressBar.Visibility = Visibility.Collapsed;
                }
                else
                {
                    StatusTextClient2.Text = $"{DateTime.Now} | Handle must be started\n" + textClient2;
                }
                StatusTextClient2.Text = textClient2;
            });
        }

        private void ClearButtonClient2_OnClick(object sender, RoutedEventArgs e)
        {
            textClient2 = string.Empty;
            StatusTextClient2.Text = string.Empty;
        }

        #endregion

    }
}
